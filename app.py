import time

import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

def get_rata(list) :
    tot = 0 
    n = len(list)
    for i in range(n) :
        tot += list[i]
        return tot/n 

def getRataGenap(list) :
    totGn = 0
    totGj = 0 
    cGn = 0 
    cGj = 0 
    n = len(list)
    for i in range(n) :
        if (list[i] % 2 == 0) : 
            totGn += list[i]
            cGn += 1
        else :
            totGj += list[i]
            cGj += 1
    return totGn/cGn , totGj/cGj




@app.route('/')
def hello():
    # count = get_hit_count()
    list1 = [10,2,4,6, 8,10,9,12]
    hasil = get_rata(list1)
    print("hasil ==")
    print(hasil)

    list2 = [10,2,4,6,8,10,9,5,12]
    hasil2, ok2 = getRataGenap(list2)
    print("genap ganjil ==")
    print(hasil2)
    print("ok2 = %f", ok2)
    return 'ok'
    # return 'Hello World! I have been seen {} times.\n'.format(count)